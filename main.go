package main

import (
	"fmt"
	"runtime/debug"
	"syscall/js"
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println(r)
			debug.PrintStack()
		}
	}()

	vis := js.Global().Get("vis")

	doc := js.Global().Get("document")
	visElement := doc.Call("getElementById", "vis")

	dataSet := vis.Get("DataSet")
	nodes := dataSet.New()

	node := js.Global().Get("Object").New()
	nodes.Call("add", node)

	edges := dataSet.New()
	datas := js.Global().Get("Object").New()
	datas.Set("nodes", nodes)
	datas.Set("edgess", edges)

	vis.Get("Network").New(
		visElement,
		datas,
		js.Global().Get("JSON").Call("parse", "{}"))
}
